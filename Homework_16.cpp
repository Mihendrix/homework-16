#include <iostream>
#include <time.h>

const int ArraySize = 5; 

int Array[ArraySize][ArraySize];

void FillArray(int array[ArraySize][ArraySize])
{
	for (size_t i = 0; i < ArraySize; i++)
	{
		for (size_t j = 0; j < ArraySize; j++)
		{
			array[i][j] = i+j;
		}
	}
}

int GetCurrentDayOfMonth()
{
	struct tm buf;
	time_t t = time(NULL);
	localtime_s(&buf, &t);
	return buf.tm_mday;
}

void PrintArray(int array[ArraySize][ArraySize])
{ 
	std::cout << "Array: " << "\n\n";

	for (size_t i = 0; i < ArraySize; i++)
	{
		for (size_t j = 0; j < ArraySize; j++)
		{
			std::cout << array[i][j];
		}
		std::cout << "\n";
	}

	std::cout << "\n";
}

void PrintSumOfArrayLineElements()
{ 
	int DayOfMonth = GetCurrentDayOfMonth();
	int RemainderOfDivision = DayOfMonth % ArraySize; 

	std::cout << "Current day of month - " << DayOfMonth << "\n\n";
	std::cout << "Array size - " << ArraySize << "\n\n";
	std::cout << "RemainderOfDivision - " << RemainderOfDivision << "\n\n";
	std::cout << "Line of array equals remainder of division - ";

	for (size_t i = 0; i < ArraySize; i++)
	{
		std::cout<< Array[RemainderOfDivision][i];
	}

	std::cout << "\n\n";

	int SumOfArrayLineElements = 0;
	for (size_t i = 0; i < ArraySize; i++)
	{
		SumOfArrayLineElements += Array[RemainderOfDivision][i];
	}

	std::cout << "Sum of elements of array at specific line - " << SumOfArrayLineElements << "\n\n";
}

int main() 
{
	FillArray(Array);
	PrintArray(Array);
	PrintSumOfArrayLineElements();
}